'use strict';

const path = require('path');

// Replace navigation links pointing to an index file with a link to its
// parent folder. That is, transform www.escio.no/some-page/index.html to
// www.escio.no/some-page/
module.exports = (config) => {
	config = Object.assign({
		index: 'index',
		extension: '.html',
		navPathProperty: 'nav_path'
	}, config || {});

	return (files, metalsmith, done) => {
		for (const filename in files) {
			if (files.hasOwnProperty(filename)) {
				const file = files[filename];
				const navPath = file[config.navPathProperty];

				// Files that aren't in any navigation groups, won't have a
				// navigation path set. If that's the case, there is not point in
				// trying to modify it:
				if (!navPath) {
					continue;
				}

				const extension = path.extname(navPath);
				const dirname = path.dirname(navPath);
				const basename = path.basename(navPath, extension);

				if (basename === config.index && extension === config.extension) {
					file[config.navPathProperty] = `${dirname}/`;
				}
			}
		}

		done();
	};
};
